let n = Number(prompt("Give me a number"));
console.log("The number you provided is " + n + ".");

for(let num = n; num > 0; num--){
	if (num <= 50){
		console.log(num);
		console.log("The current value is at 50 (or less). I'm terminating the loop.");
		break;
	}	

	if (num % 10 === 0) {
		console.log(num);
		console.log("The number is divisible by 10. Skipping...");
		continue;
	}

	console.log(num);

	if (num % 5 === 0) {
		console.log(num);;
	}
}

let w = "pneumonoultramicroscopicsilicovolcanoconiosis";
let w1 = "";

for(let x = 0; x < w.length; x++){

	if(
		w[x].toLowerCase() == "a" ||
		w[x].toLowerCase() == "e" ||
		w[x].toLowerCase() == "i" ||
		w[x].toLowerCase() == "o" ||
		w[x].toLowerCase() == "u" 
	){
		continue;
	}
	else{
		w1 += w[x];
	}
}
console.log(w);
console.log(w1);